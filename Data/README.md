# The CERBERE data release

In this folder you will find all data relating to the CERBERE exercise. Each file and folder's contents are described hereafter:

- *raw\_data.zip* is an archive containing all of the CERBERE data collected during the red team part of the event. It is organised by instance deployed and then by machine. Each machine in turn contains its own auditd logs and suricata logs.
- The *System_logs_annotations* folder contains:
	- a *graphs* folder with images recapitulating the portions of the scenario sccuessfully executed and detected in the logs for the corresponding instance 
	- a folder for each scenario containing json files for each machine, recapitulating the log lines identifying attacks.

# Data structure

The raw data is extracted from two different sources: auditd and Suricata. In the next two sections we give out more information about how to read these logs. Since the auditd logs are system side and the suricata logs are network side, the information found in each is different by design. Sometimes the data will be redundant, such as in cases where an exploit executed on a machine goes through a remote command. However, some behaviors, particularly the way attackers move from machine to machine will be easily observed from the suricata logs when auditd can only focus on its own machine. On the other hand the auditd logs will capture anything done directly on a machine and not over the network. These differences are the reason why we setup both kinds of monitoring for this exercise. 

## Auditd logs

the red hat guide section about [understanding audit log files](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/security_guide/sec-understanding_audit_log_files) does a good job of explaining how you can read the auditd logs. However, we want to give some precisions about these raw logs:

- Auditd logs can be stored on multiple lines. The way to know which logs are part of the same alert is to compare the *msg* field (*e.g.*: `msg=audit(1681300801.276:12873)` where this identifier can be shared between log lines, meaning they are one same alert.) There is a solution to reformat auditd logs: using logstash along with filebeat in order to dump them in a better structured json format.
- Auditd logs often have two different *msg* fields, the first one is the log identifier and the second one is the actual message caught when the log is saved.
- The *key* field present in every line can be matched with a rule present in the configuration file in order to better understand the conditions for catching this alert.
- The *type* field will rapidly give the information of the level at which the log was captured: process level (*PROCTITLE*) or syscall level (*SYSCALL*).

## Suricata logs

The suricata folders are more complex, but there are three files that can be of interest: 
- *eve.json*: contains the suricata events structured in json objects. It is to be noted that this file's json objects can be nested. 
- *log.pcap.\**: the network packets capture version of the events caught by suricata.
- *suricata.log*: The logs in a more traditional log format file.
